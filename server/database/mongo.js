"use strict";
var mongoose = require('mongoose');
var ConnectionState = require('./connection-state.enum');
var Mongo = (function () {
    function Mongo(conStr) {
        this.connectionString = conStr || 'mongodb://localhost/angularattack';
    }
    Mongo.prototype.connect = function () {
        mongoose.connect(this.connectionString, function (err) {
            if (err) {
                console.log('Could not connect to Mongo!!');
                console.log(err.name);
                console.log(err.message);
            }
        });
    };
    Mongo.prototype.status = function () {
        // const connections = mongoose.connections;
        var connections = null;
        if (!connections) {
            return ConnectionState.DEAD;
        }
        else {
            for (var i = 0; i < connections.length; i++) {
                var connection = connections[i];
                if (!connection.host ||
                    !connection.port ||
                    !connection.name ||
                    connection._readyState === 0) {
                    return ConnectionState.DEAD;
                }
            }
            return ConnectionState.ALIVE;
        }
    };
    return Mongo;
}());
module.exports = Mongo;
//# sourceMappingURL=mongo.js.map