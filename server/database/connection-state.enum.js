"use strict";
var ConnectionState;
(function (ConnectionState) {
    ConnectionState[ConnectionState["ALIVE"] = 0] = "ALIVE";
    ConnectionState[ConnectionState["DEAD"] = 1] = "DEAD";
})(ConnectionState || (ConnectionState = {}));
module.exports = ConnectionState;
//# sourceMappingURL=connection-state.enum.js.map