"use strict";
var express_1 = require('express');
var CoreController = require('./controllers/core.controller');
/**
 * Define routes for the express application
 */
var router = express_1.Router();
var coreController = new CoreController();
router.get('/', coreController.list);
module.exports = router;
//# sourceMappingURL=routes.js.map