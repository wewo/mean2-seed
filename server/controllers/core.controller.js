"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var BaseController = require('./base.controller');
var CoreController = (function (_super) {
    __extends(CoreController, _super);
    function CoreController() {
        _super.apply(this, arguments);
    }
    CoreController.prototype.list = function (req, res) {
        if (process.env.NODE_ENV === 'testing') {
            res.render('ng-unit-tests');
        }
        else {
            res.render('index');
        }
    };
    return CoreController;
}(BaseController));
module.exports = CoreController;
//# sourceMappingURL=core.controller.js.map