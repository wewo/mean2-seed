"use strict";
var BaseController = (function () {
    function BaseController() {
    }
    BaseController.prototype.list = function (req, res) {
        throw new Error('Not implemented');
    };
    BaseController.prototype.create = function (req, res) {
        throw new Error('Not implemented');
    };
    BaseController.prototype.read = function (req, res) {
        throw new Error('Not implemented');
    };
    BaseController.prototype.update = function (req, res) {
        throw new Error('Not implemented');
    };
    BaseController.prototype.delete = function (req, res) {
        throw new Error('Not implemented');
    };
    return BaseController;
}());
module.exports = BaseController;
//# sourceMappingURL=base.controller.js.map