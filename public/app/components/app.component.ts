import {Component} from '@angular/core';

@Component({
	selector: 'lk-app',
	template: '<h1>My First Angular2 App</h1>'
})
export class AppComponent {}
